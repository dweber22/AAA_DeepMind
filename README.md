# AAA DeepMind

Project for AAA course

## Installation

Please unzip the "data.zip" file before running the code.

A conda env file is included.

you can import it into conda by using the command:

conda env create --file env.txt

from within the repos directory.


## Usage

Once the "data.zip" is unzipped, any notebook can be run individually.

If you use your own data, use the data cleaning file first, just make sure that all your data files and contents are named correctly.


## Authors 

Damian Weber

Thomas Rohrsdorfer

Lishan Soosaisanther

Jiarun Liu

Matthias Lehmann 

Lina Abed Isa